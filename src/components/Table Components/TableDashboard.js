import React from "react";

function TableDashboard() {
  return (
    <div>
      <div className="container table-responsive py-5">
        <h3
          className="text-center p-3 text-white"
          style={{ backgroundColor: "#2574A9" }}
        >
          Riwayat Pasien
        </h3>
        <table
          className="table table-bordered table-hover"
          style={{ borderStyle: "none" }}
        >
          <thead className="thead-dark" style={{ backgroundColor: "#2574A9" }}>
            <tr className="text-white">
              <th scope="col">No</th>
              <th scope="col">Nama Pasien</th>
              <th scope="col">Status Pasien</th>
              <th scope="col">Tanggal/Jam Periksa</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row"></th>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TableDashboard;
