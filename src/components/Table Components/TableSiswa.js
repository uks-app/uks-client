import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { instance as axios } from "../../util/api";
import AddSiswa from "../add/AddSiswa";
import { Link } from "react-router-dom";

function TableSiswa() {
  const [siswa, setSiswa] = useState([]);

  const fetchSiswa = async () => {
    try {
      const { data, status } = await axios.get(`siswa`, {});
      if (status === 200) {
        setSiswa(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchSiswa();
  }, []);

  const removePost = async (id) => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/siswa/${id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div>
      <div className="container table-responsive py-5">
        <AddSiswa />
        <table
          className="table table-bordered table-hover"
          style={{ borderStyle: "none" }}
        >
          <thead className="thead-dark" style={{ backgroundColor: "#2574A9" }}>
            <tr className="text-white">
              <th scope="col">No</th>
              <th scope="col">Nama Siswa</th>
              <th scope="col">Tempat Tanggal Lahir</th>
              <th scope="col">Alamat</th>
              <th scope="col">Kelas</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {siswa.map((siswa, index) => (
              <tr key={index} className="text-dark">
                <th scope="row">{siswa.id}</th>
                <td>{siswa.siswa}</td>
                <td>{siswa.lahir}</td>
                <td>{siswa.alamat}</td>
                <td>{siswa.kelas}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => removePost(siswa.id)}
                  >
                    <i className="fa-solid fa-trash"></i>
                  </button>
                  <Link
                    className="btn btn-primary mx-2"
                    to={`/updatesiswa/${siswa.id}`}
                  >
                    <i className="fa-solid fa-pen-to-square"></i>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TableSiswa;
