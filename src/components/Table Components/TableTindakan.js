import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { instance as axios } from "../../util/api";
import { Link } from "react-router-dom";
import AddTindakan from "../add/AddTindakan";

function TableTindakan() {
  const [tindakan, setTindakan] = useState([]);

  const fetchTindakan = async () => {
    try {
      const { data, status } = await axios.get(`tindakan`, {});
      if (status === 200) {
        setTindakan(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchTindakan();
  }, []);

  const removePost = async (id) => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/tindakan/${id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div>
      <div className="container table-responsive py-5">
        <AddTindakan />
        <table
          className="table table-bordered table-hover"
          style={{ borderStyle: "none" }}
        >
          <thead className="thead-dark" style={{ backgroundColor: "#2574A9" }}>
            <tr className="text-white">
              <th scope="col">No</th>
              <th scope="col">tindakan</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {tindakan.map((tindakan, index) => (
              <tr key={index} className="text-dark">
                <th scope="row">{tindakan.id}</th>
                <td>{tindakan.tindakan}</td>
                <td>
                  {/* hapus */}
                  <button
                    className="btn btn-danger"
                    onClick={() => removePost(tindakan.id)}
                  >
                    <i className="fa-solid fa-trash"></i>
                  </button>

                  {/* update */}
                  <Link
                    className="btn btn-primary mx-2"
                    to={`/updatetindakan/${tindakan.id}`}
                  >
                    <i className="fa-solid fa-pen-to-square"></i>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TableTindakan;
