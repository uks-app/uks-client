import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { instance as axios } from "../../util/api";
import AddDiagnosa from "../add/AddDiagnosa";
import { Link } from "react-router-dom";

function TableDiagnosa() {
  const [diagnosa, setDiagnosa] = useState([]);

  const fetchDiagnosa = async () => {
    try {
      const { data, status } = await axios.get(`diagnosa`, {});
      if (status === 200) {
        setDiagnosa(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchDiagnosa();
  }, []);

  const removePost = async (id) => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/diagnosa/${id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div>
      <div className="container table-responsive py-5">
        <AddDiagnosa />
        <table
          className="table table-bordered table-hover"
          style={{ borderStyle: "none" }}
        >
          <thead className="thead-dark" style={{ backgroundColor: "#2574A9" }}>
            <tr className="text-white">
              <th scope="col">No</th>
              <th scope="col">Diagnosa</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {diagnosa.map((diagnosa, index) => (
              <tr key={index} className="text-dark">
                <th scope="row">{diagnosa.id}</th>
                <td>{diagnosa.diagnosa}</td>
                <td>
                  {/* hapus */}
                  <button
                    className="btn btn-danger"
                    onClick={() => removePost(diagnosa.id)}
                  >
                    <i className="fa-solid fa-trash"></i>
                  </button>

                  {/* update */}
                  <Link
                    className="btn btn-primary mx-2"
                    to={`/updatediagnosa/${diagnosa.id}`}
                  >
                    <i className="fa-solid fa-pen-to-square"></i>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TableDiagnosa;
