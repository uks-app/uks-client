import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { instance as axios } from "../../util/api";
import AddGuru from "../add/AddGuru";
import UpdateGuru from "../update/UpdateGuru";
import { Link } from "react-router-dom";

function TableGuru() {
  const [guru, setGuru] = useState([]);

  const fetchGuru = async () => {
    try {
      const { data, status } = await axios.get(`guru`, {});
      if (status === 200) {
        setGuru(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGuru();
  }, []);

  const removePost = async (id) => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/guru/${id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div>
      <div className="container table-responsive py-5">
        <AddGuru />
        <table
          className="table table-bordered table-hover"
          style={{ borderStyle: "none" }}
        >
          <thead className="thead-dark" style={{ backgroundColor: "#2574A9" }}>
            <tr className="text-white">
              <th scope="col">No</th>
              <th scope="col">Nama Guru</th>
              <th scope="col">Tempat Tanggal Lahir</th>
              <th scope="col">Alamat</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {guru.map((guru, index) => (
              <tr key={index} className="text-dark">
                <th scope="row">{guru.id}</th>
                <td>{guru.guru}</td>
                <td>{guru.lahir}</td>
                <td>{guru.alamat}</td>
                <td>
                  {/* hapus */}
                  <button
                    className="btn btn-danger"
                    onClick={() => removePost(guru.id)}
                  >
                    <i className="fa-solid fa-trash"></i>
                  </button>

                  {/* update */}
                  <Link
                    className="btn btn-primary mx-2"
                    to={`/updateguru/${guru.id}`}
                  >
                    <i className="fa-solid fa-pen-to-square"></i>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TableGuru;
