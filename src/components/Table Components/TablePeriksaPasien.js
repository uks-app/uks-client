import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { instance as axios } from "../../util/api";
// import { Link } from "react-router-dom";
import AddPeriksaPasien from "../add/AddPeriksaPasien";

function TablePeriksaPasien() {
  const [periksa, setPeriksa] = useState([]);

  const fetchPeriksa = async () => {
    try {
      const { data, status } = await axios.get(`periksa`, {});
      if (status === 200) {
        setPeriksa(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchPeriksa();
  }, []);

  const removePost = async (id) => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/periksa/${id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div>
      <div className="container table-responsive py-5">
        <AddPeriksaPasien />
        <table
          className="table table-bordered table-hover"
          style={{ borderStyle: "none" }}
        >
          <thead className="thead-dark" style={{ backgroundColor: "#2574A9" }}>
            <tr className="text-white">
              <th scope="col">No</th>
              <th scope="col">Nama Pasien</th>
              <th scope="col">Status Pasien</th>
              <th scope="col">Jabatan</th>
              <th scope="col">Keterangan</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {periksa.map((periksa, index) => (
              <tr key={index} className="text-dark">
                <th scope="row">{periksa.id}</th>
                <td>{periksa.periksa}</td>
                <td>{periksa.lahir}</td>
                <td>{periksa.alamat}</td>
                <td>{periksa.keterangan}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => removePost(periksa.id)}
                  >
                    Tangani
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TablePeriksaPasien;
