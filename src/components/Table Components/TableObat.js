import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { instance as axios } from "../../util/api";
import { Link } from "react-router-dom";
import AddObat from "../add/AddObat";

function TableObat() {
  const [obat, setObat] = useState([]);

  const fetchobat = async () => {
    try {
      const { data, status } = await axios.get(`obat`, {});
      if (status === 200) {
        setObat(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchobat();
  }, []);

  const removePost = async (id) => {
    await Swal.fire({
      title: "Do You Want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/obat/${id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Succesfully Deleted Your Post",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "canceled",
          text: "",
          howConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div>
      <div className="container table-responsive py-5">
        <AddObat />
        <table
          className="table table-bordered table-hover"
          style={{ borderStyle: "none" }}
        >
          <thead className="thead-dark" style={{ backgroundColor: "#2574A9" }}>
            <tr className="text-white">
              <th scope="col">No</th>
              <th scope="col">Obat</th>
              <th scope="col">Stock</th>
              <th scope="col">Tanggal Expired</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {obat.map((obat, index) => (
              <tr key={index} className="text-dark">
                <th scope="row">{obat.id}</th>
                <td>{obat.obat}</td>
                <td>{obat.stock}</td>
                <td>{obat.expired}</td>
                <td>
                  {/* hapus */}
                  <button
                    className="btn btn-danger"
                    onClick={() => removePost(obat.id)}
                  >
                    <i className="fa-solid fa-trash"></i>
                  </button>

                  {/* update */}
                  <Link
                    className="btn btn-primary mx-2"
                    to={`/updateobat/${obat.id}`}
                  >
                    <i className="fa-solid fa-pen-to-square"></i>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TableObat;
