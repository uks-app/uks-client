import React from "react";
import Navbar from "./Navbar";
import TablePenanganan from "./Table Components/TablePenanganan";

function PenangananPertama() {
  return (
    <>
      <Navbar />

      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}
          <div>
            <TablePenanganan />
          </div>
          <div className="jumbotron"></div>
        </div>
      </div>
    </>
  );
}

export default PenangananPertama;
