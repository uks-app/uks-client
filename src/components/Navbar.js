import React from "react";
import "../css/Navbar.css";
import { Link, useNavigate } from "react-router-dom";
import Clock from "./Clock";

function Navbar() {
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };
  return (
    <>
      <div>
        <div className="sidebar-container">
          <div className="sidebar-logo">Project Name</div>
          <ul className="sidebar-navigation">
            <li className="header">Navigation</li>
            <li>
              <Link style={{ textDecoration: "none" }} to="/" href="#">
                <i className="fa fa-tachometer" aria-hidden="true"></i>
                Dashboard
              </Link>
            </li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/periksapasien"
                href="#"
              >
                <i className="fa-sharp fa-solid fa-stethoscope"></i>
                Periksa Pasien
              </Link>
            </li>
            <li className="header">Data</li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/daftarguru"
                href="#"
              >
                <i className="fa-solid fa-graduation-cap"></i>
                Daftar Guru
              </Link>
            </li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/daftarsiswa"
                href="#"
              >
                <i className="fa-solid fa-user"></i> Daftar Siswa
              </Link>
            </li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/daftarkaryawan"
                href="#"
              >
                <i className="fa fa-users" aria-hidden="true"></i>
                Daftar Karyawan
              </Link>
            </li>
            <li className="header">Action</li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/daftardiagnosa"
                href="#"
              >
                <i className="fa-solid fa-person-dots-from-line"></i> Diagnosa
              </Link>
            </li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/daftarpenanganan"
                href="#"
              >
                <i className="fa-sharp fa-solid fa-location-crosshairs"></i>
                Penanganan Pertama
              </Link>
            </li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/daftartindakan"
                href="#"
              >
                <i className="fa-sharp fa-solid fa-location-crosshairs"></i>{" "}
                Tindakan
              </Link>
            </li>
            <li>
              <Link
                style={{ textDecoration: "none" }}
                to="/daftarobat"
                href="#"
              >
                <i className="fa-solid fa-pills"></i> Daftar Obat P3K
              </Link>
            </li>
          </ul>
          <div className="d-flex justify-content-center">
            <div
              className="my-2 mx-2"
              // onClick={logout}
            >
              <Clock />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Navbar;
