import React from "react";
import Navbar from "./Navbar";
import TableKaryawan from "./Table Components/TableKaryawan";

function DaftarKaryawan() {
  return (
    <>
      <Navbar />

      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}
          <div>
            <TableKaryawan />
          </div>
          <div className="jumbotron"></div>
        </div>
      </div>
    </>
  );
}

export default DaftarKaryawan;
