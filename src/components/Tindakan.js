import React from "react";
import Navbar from "./Navbar";
import TableTindakan from "./Table Components/TableTindakan";

function Tindakan() {
  return (
    <>
      <Navbar />

      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}
          <div>
            <TableTindakan />
          </div>
          <div className="jumbotron"></div>
        </div>
      </div>
    </>
  );
}

export default Tindakan;
