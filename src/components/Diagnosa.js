import React from "react";
import Navbar from "./Navbar";
import TableDiagnosa from "./Table Components/TableDiagnosa";

function Diagnosa() {
  return (
    <>
      <Navbar />

      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}
          <div>
            <TableDiagnosa />
          </div>
          <div className="jumbotron"></div>
        </div>
      </div>
    </>
  );
}

export default Diagnosa;
