import React from "react";
import Navbar from "./Navbar";
import TableSiswa from "./Table Components/TableSiswa";

function DaftarSiswa() {
  return (
    <>
      <Navbar />

      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}
          <TableSiswa />
          <div className="jumbotron"></div>
        </div>
      </div>
    </>
  );
}

export default DaftarSiswa;
