import React, { useState } from "react";
import Navbar from "./Navbar";
import TableGuru from "./Table Components/TableGuru";

function DaftarGuru() {
  return (
    <>
      <Navbar />

      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}

          <TableGuru />

          <div className="jumbotron"></div>
        </div>
      </div>
    </>
  );
}

export default DaftarGuru;
