import React, { useEffect, useState } from "react";

const Clock = () => {
  const [time, setTime] = useState("00:00:00 XM");
  const [date, setDate] = useState("day, month 00, 0000");

  useEffect(() => {
    setInterval(() => {
      const dt = new Date();

      const dayNames = [
        "Minggu",
        "Senin",
        "Selasa",
        "Rabu",
        "Kamis",
        "Jumat",
        "Sabtu",
      ];
      const monthNames = [
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Augustus",
        "September",
        "Oktober",
        "November",
        "Desember",
      ];

      // Time
      let hour = dt.getHours();
      let minute = dt.getMinutes();
      let second = dt.getSeconds();

      let xm = hour >= 12 ? "PM" : "AM";

      hour = hour > 12 ? hour - 12 : hour;
      hour = hour === 0 ? (hour = 12) : hour;
      hour = hour < 10 ? "0" + hour : hour;
      minute = minute < 10 ? "0" + minute : minute;
      second = second < 10 ? "0" + second : second;

      let newTime = `${hour}:${minute}:${second} ${xm}`;
      setTime(newTime);

      // Date
      let dayIndex = dt.getDay();
      let dayName = dayNames[dayIndex];

      let monthIndex = dt.getMonth();
      let monthName = monthNames[monthIndex];

      let today = dt.getDate();
      let year = dt.getFullYear();

      today = today < 10 ? "0" + today : today;

      let newDate = `${dayName},  ${today} ${monthName}, ${year}`;
      setDate(newDate);
    }, 1000);
  }, [time, date]);

  return (
    <div className="clock mt-3">
      <h3 className="text-center">{time}</h3>
      <p className="text-center">{date}</p>
    </div>
  );
};

export default Clock;
