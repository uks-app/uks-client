import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import "../css/Register.css";

function Register() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userRegister, setUserRegister] = useState({
    username: "",
    password: "",
    role: "",
  });

  const handleOnChange = (e) => {
    setUserRegister((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signUp = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userRegister),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.jwtToken);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/login");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <>
      <div className="login-container text-c animated flipInX">
        <div>
          <h1 className="logo-badge text-whitesmoke">
            <img
              src="https://binusasmg.sch.id/ppdb/logobinusa.png"
              className="rounded mx-auto d-block my-2"
              height="100px"
              width="120px"
            ></img>
          </h1>
        </div>
        <h3 className="text-whitesmoke">Sistem Aplikasi UKS</h3>
        <p className="text-whitesmoke">SMK BINA NUSANTARA DEMAK</p>
        <div className="container-content">
          <form className="margin-t">
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                placeholder="Username"
                required
                id="username"
                onChange={handleOnChange}
                value={userRegister.username}
              />
            </div>
            <div className="form-group">
              <input
                type="password"
                className="form-control"
                placeholder="Password"
                required
                id="password"
                onChange={handleOnChange}
                value={userRegister.password}
              />
            </div>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                placeholder="Role"
                required
                id="role"
                onChange={handleOnChange}
                value={userRegister.role}
              />
            </div>
            <button onClick={signUp} className="form-button button-l margin-b">
              Daftar
            </button>

            <Link to="/login" className="text-whitesmoke text-center">
              <small>apakah kamu sudah punya akun?</small>
            </Link>
          </form>
        </div>
      </div>
    </>
  );
}

export default Register;
