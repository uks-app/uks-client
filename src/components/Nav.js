import React from "react";
import Clock from "./Clock";

function Nav() {
  return (
    <div>
      <ul class="nav justify-content-center fixed-top">
        <li class="nav-item">
          <a
            class="nav-link disabled"
            href="#"
            tabindex="-1"
            aria-disabled="true"
          >
            <Clock />
          </a>
        </li>
      </ul>
    </div>
  );
}

export default Nav;
