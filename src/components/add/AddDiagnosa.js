import { instance as axios } from "../../util/api";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

function AddDiagnosa() {
  const [diagnosa, setDiagnosa] = useState("");
  const navigate = useNavigate();

  const postDiagnosa = async () => {
    try {
      const formData = {
        diagnosa: diagnosa,
      };
      await axios.post(`diagnosa/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/daftardiagnosa");
  };

  const save = (e) => {
    e.preventDefault();
    postDiagnosa();
    Swal.fire({
      icon: "success",
      title: "Succes Add Book",
      showConfirmButton: false,
      timer: 800,
    });
  };

  function refreshPage() {
    window.location.reload(false);
  }

  return (
    <div>
      <div
        className="d-flex justify-content-between"
        style={{ backgroundColor: "#2574A9" }}
      >
        <span></span>
        <h3 className="text-center p-3 text-white">Daftar diagnosa</h3>
        <button
          type="button"
          className="btn btn-success btn-lg float-right m-2"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          <i className="fa-sharp fa-solid fa-plus"></i> Add
        </button>

        {/* modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title" id="exampleModalLabel">
                  Add Diagnosa
                </h4>
                <button
                  type="button"
                  className="close btn btn-danger"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={save}>
                  <div className="form-group">
                    <label>Diagnosa</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={diagnosa}
                      onChange={(e) => setDiagnosa(e.target.value)}
                    />
                  </div>
                  <div className="modal-footer">
                    <button
                      onClick={refreshPage}
                      type="button submit"
                      className="btn btn-success"
                    >
                      Tambah
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      Close
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddDiagnosa;
