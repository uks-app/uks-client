import { instance as axios } from "../../util/api";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import SelectStatus from "../SelectStatus";

function AddPeriksaPasien() {
  const [keterangan, setKeterangan] = useState("");
  const [status, setStatus] = useState([""]);
  const navigate = useNavigate();

  //status
  const fetchStatus = async () => {
    try {
      const { data, status } = await axios.get(`status`, {});
      if (status === 200) {
        setStatus(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchStatus();
  }, []);
  const postPeriksaPasien = async () => {
    try {
      const formData = {
        keterangan: keterangan,
        status: status,
      };
      await axios.post(`periksa/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/daftarperiksa");
  };

  const save = (e) => {
    e.preventDefault();
    postPeriksaPasien();
    Swal.fire({
      icon: "success",
      title: "Succes Add periksa",
      showConfirmButton: false,
      timer: 800,
    });
  };

  function refreshPage() {
    window.location.reload(false);
  }

  return (
    <div>
      <div
        className="d-flex justify-content-between"
        style={{ backgroundColor: "#2574A9" }}
      >
        <span></span>
        <h3 className="text-center p-3 text-white">Daftar Periksa Pasien</h3>
        <button
          type="button"
          className="btn btn-success btn-lg float-right m-2"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          <i className="fa-sharp fa-solid fa-plus"></i> Add
        </button>

        {/* modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title" id="exampleModalLabel">
                  Add Guru
                </h4>
                <button
                  type="button"
                  className="close btn btn-danger"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={save}>
                  <div className="form-group">
                    <label>Nama keterangan</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={keterangan}
                      onChange={(e) => setKeterangan(e.target.value)}
                    />

                    <label>Status</label>
                    <select
                      class="form-select"
                      aria-label="Default select example"
                    >
                      <option selected>Open this select menu</option>
                      {status.map((status, index) => (
                        <option
                          value={keterangan}
                          onChange={(e) => setKeterangan(e.target.value)}
                          key={index}
                        >
                          {status.status}
                        </option>
                      ))}
                    </select>

                    {/* <label>Tempat Tanggal Lahir</label>
                    <input
                      type="date"
                      className="form-control"
                      placeholder="..."
                      value={lahir}
                      onChange={(e) => setLahir(e.target.value)}
                    />
                    <label>Alamat</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={alamat}
                      onChange={(e) => setAlamat(e.target.value)}
                    />

                    <label>Kelas</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={kelas}
                      onChange={(e) => setKelas(e.target.value)}
                    /> */}
                  </div>
                  <div className="modal-footer">
                    <button
                      onClick={refreshPage}
                      type="button submit"
                      className="btn btn-success"
                    >
                      Tambah
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      Close
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddPeriksaPasien;
