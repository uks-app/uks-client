import { instance as axios } from "../../util/api";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

function UpdateTindakan() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [tindakan, setTindakan] = useState("");

  const putTindakan = async (e) => {
    e.preventDefault();
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        tindakan: tindakan,
      };
      console.log(data);

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`tindakan/${id}`, data);

          navigate("/daftartindakan");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const getDataById = async () => {
    const { data } = await axios.get(`tindakan/id/${id}`);
    console.log(data);

    setTindakan(data.tindakan);

    // console.log(data);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div className="container">
      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}

          <div className="container mt-3">
            <h1 className="text-center p-3">Form Update</h1>
            <form onSubmit={putTindakan}>
              {/* <!-- tindakan input --> */}
              <div className="form-outline mb-4">
                <input
                  type="text"
                  id="form3Example3"
                  className="form-control"
                  defaultValue={tindakan}
                  onChange={(e) => setTindakan(e.target.value)}
                />
                <label className="form-label" for="form3Example3">
                  Nama tindakan
                </label>
              </div>

              {/* <!-- Submit button --> */}
              <button type="submit" className="btn btn-primary btn-block mb-4">
                Update
              </button>

              {/* <!-- Register buttons --> */}
            </form>
          </div>

          <div className="jumbotron"></div>
        </div>
      </div>
    </div>
  );
}

export default UpdateTindakan;
