import { instance as axios } from "../../util/api";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

function UpdatePenanganan() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [penanganan, setPenanganan] = useState("");

  const putPenanganan = async (e) => {
    e.preventDefault();
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        penanganan: penanganan,
      };
      console.log(data);

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`penanganan/${id}`, data);

          navigate("/daftarpenanganan");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const getDataById = async () => {
    const { data } = await axios.get(`penanganan/id/${id}`);
    console.log(data);

    setPenanganan(data.penanganan);

    // console.log(data);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div className="container">
      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}

          <div className="container mt-3">
            <h1 className="text-center p-3">Form Update</h1>
            <form onSubmit={putPenanganan}>
              {/* <!-- penanganan input --> */}
              <div className="form-outline mb-4">
                <input
                  type="text"
                  id="form3Example3"
                  className="form-control"
                  defaultValue={penanganan}
                  onChange={(e) => setPenanganan(e.target.value)}
                />
                <label className="form-label" for="form3Example3">
                  Nama penanganan
                </label>
              </div>

              {/* <!-- Submit button --> */}
              <button type="submit" className="btn btn-primary btn-block mb-4">
                Update
              </button>

              {/* <!-- Register buttons --> */}
            </form>
          </div>

          <div className="jumbotron"></div>
        </div>
      </div>
    </div>
  );
}

export default UpdatePenanganan;
