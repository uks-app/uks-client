import { instance as axios } from "../../util/api";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

function UpdateSiswa() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [siswa, setSiswa] = useState("");
  const [lahir, setLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [kelas, setKelas] = useState("");

  const putGuru = async (e) => {
    e.preventDefault();
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        siswa: siswa,
        lahir: lahir,
        alamat: alamat,
        kelas: kelas,
      };
      console.log(data);

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`siswa/${id}`, data);

          navigate("/daftarsiswa");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  //   const save = () => {
  //     putGuru();
  //   };

  const getDataById = async () => {
    const { data } = await axios.get(`siswa/id/${id}`);
    console.log(data);

    setSiswa(data.siswa);
    setLahir(data.lahir);
    setAlamat(data.alamat);
    setKelas(data.kelas);
    // console.log(data);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div className="container">
      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}

          <div className="container mt-3">
            <h1 className="text-center p-3">Form Update</h1>
            <form onSubmit={putGuru}>
              {/* <!-- siswa input --> */}
              <div className="form-outline mb-4">
                <input
                  type="text"
                  id="form3Example3"
                  className="form-control"
                  defaultValue={siswa}
                  onChange={(e) => setSiswa(e.target.value)}
                />
                <label className="form-label" for="form3Example3">
                  Nama Guru
                </label>
              </div>

              {/* <!-- tempat tanggal lahir input --> */}
              <div className="form-outline mb-4">
                <input
                  type="text"
                  id="form3Example4"
                  className="form-control"
                  defaultValue={lahir}
                  onChange={(e) => setLahir(e.target.value)}
                />
                <label className="form-label" for="form3Example4">
                  Tempat Tanggal Lahir
                </label>
              </div>

              {/* <!-- Alamat input --> */}
              <div className="form-outline mb-4">
                <input
                  type="text"
                  id="form3Example4"
                  className="form-control"
                  defaultValue={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                />
                <label className="form-label" for="form3Example4">
                  Alamat
                </label>
              </div>

              {/* <!-- Kelas input --> */}
              <div className="form-outline mb-4">
                <input
                  type="text"
                  id="form3Example4"
                  className="form-control"
                  defaultValue={kelas}
                  onChange={(e) => setKelas(e.target.value)}
                />
                <label className="form-label" for="form3Example4">
                  Kelas
                </label>
              </div>

              {/* <!-- Submit button --> */}
              <button type="submit" className="btn btn-primary btn-block mb-4">
                Update
              </button>

              {/* <!-- Register buttons --> */}
            </form>
          </div>

          <div className="jumbotron"></div>
        </div>
      </div>
    </div>
  );
}

export default UpdateSiswa;
