import React, { useState } from "react";
import "../css/Login.css";
import Swal from "sweetalert2";
import { useLocation, useNavigate } from "react-router-dom";
// import axios from "axios";

function Login() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userLogin, setUserLogin] = useState({ email: "", password: "" });

  const handleOnChange = (e) => {
    setUserLogin((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signIn = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userLogin),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);
        localStorage.setItem("id", data.userData.id);
        localStorage.setItem("username", data.userData.username);
        localStorage.setItem("role", data.userData.role);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/");
          Swal.fire({
            icon: "success",
            title: "Yeayy your Login Success",
            showConfirmButton: false,
            timer: 800,
          });
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Email or Password Wrong",
          showConfirmButton: false,
          timer: 800,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className=" text-white reg">
      <div className="container-fluid p-1">
        <h4 className="text-center mt-3">Sistem Aplikasi UKS</h4>
        <img
          src="https://binusasmg.sch.id/ppdb/logobinusa.png"
          className="rounded mx-auto d-block my-2"
          height="100px"
          width="120px"
        ></img>
        <div className="d-flex justify-content-center mx-3 mb-5">
          <div className="form-bg mb-3">
            <div className="container">
              <div className="row ">
                <div className="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 ">
                  <div className="form-container">
                    <form className="form-horizontal">
                      <h3 className="title">User Login</h3>
                      <div className="form-group">
                        <span className="input-icon">
                          <i className="fa fa-user"></i>
                        </span>
                        <input
                          className="form-control"
                          type="email"
                          placeholder="Username"
                          id="username"
                          onChange={handleOnChange}
                          value={userLogin.username}
                        />
                      </div>
                      <div className="form-group">
                        <span className="input-icon">
                          <i className="fa fa-lock"></i>
                        </span>
                        <input
                          className="form-control"
                          type="password"
                          id="password"
                          placeholder="Password"
                          onChange={handleOnChange}
                          value={userLogin.password}
                        />
                      </div>
                      <span className="forgot-pass">
                        <a href="#">Lost password?</a>
                      </span>
                      <button
                        className="btn signin"
                        type="submit"
                        onClick={signIn}
                      >
                        Login
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <h2 className="text-dark">.</h2>
        <h5 className="text-dark">.</h5>
      </div>
    </div>
  );
}

export default Login;
