import React from "react";
import Navbar from "./Navbar";
import TableDashboard from "./Table Components/TableDashboard";
import Clock from "./Clock";

function Dashboard() {
  return (
    <>
      <Navbar />
      <div>
        <div className="content-container">
          <div className="container-fluid">
            {/* <!-- Main component for a primary marketing message or call to action --> */}

            <div className="row  d-flex text-center p-3">
              <h3 className="text-center p-3">DASHBOARD</h3>

              <div className="col-4 ">
                <div
                  style={{ borderRadius: "0%", border: "3px solid #2574A9" }}
                  className="card"
                >
                  <div className="card-body">
                    <h5 className="card-title">Daftar Pasien</h5>
                    <p className="card-text fs-2" style={{ color: "#2574A9" }}>
                      <i
                        style={{
                          border: "10px solid #2574A9",
                          borderRadius: "50%",
                          backgroundColor: "#2574A9",
                        }}
                        className="fa-solid fa-wheelchair text-white mx-2 fs-3"
                      ></i>
                      0 Guru
                    </p>
                  </div>
                </div>
              </div>
              {/* ke 2 */}
              <div className="col-4 ">
                <div
                  style={{ borderRadius: "0%", border: "3px solid #2574A9" }}
                  className="card"
                >
                  <div className="card-body">
                    <h5 className="card-title">Daftar Pasien</h5>
                    <p className="card-text fs-2" style={{ color: "#2574A9" }}>
                      <i
                        style={{
                          border: "10px solid #2574A9",
                          borderRadius: "50%",
                          backgroundColor: "#2574A9",
                        }}
                        className="fa-solid fa-wheelchair text-white mx-2 fs-3"
                      ></i>
                      0 Siswa
                    </p>
                  </div>
                </div>
              </div>
              {/* ke 3 */}
              <div className="col-4 ">
                <div
                  style={{ borderRadius: "0%", border: "3px solid #2574A9" }}
                  className="card"
                >
                  <div className="card-body">
                    <h5 className="card-title">Daftar Pasien</h5>
                    <p className="card-text fs-2" style={{ color: "#2574A9" }}>
                      <i
                        style={{
                          border: "10px solid #2574A9",
                          borderRadius: "50%",
                          backgroundColor: "#2574A9",
                        }}
                        className="fa-solid fa-wheelchair text-white mx-2 fs-3"
                      ></i>
                      0 Karyawan
                    </p>
                  </div>
                </div>
              </div>
              <TableDashboard />
            </div>
          </div>

          <div className="jumbotron mx-4 text-end"></div>
        </div>
      </div>
    </>
  );
}

export default Dashboard;
