import React from "react";
import Navbar from "./Navbar";
import TableObat from "./Table Components/TableObat";

function Obat() {
  return (
    <>
      <Navbar />

      <div className="content-container">
        <div className="container-fluid">
          {/* <!-- Main component for a primary marketing message or call to action --> */}
          <div>
            <TableObat />
          </div>
          <div className="jumbotron"></div>
        </div>
      </div>
    </>
  );
}

export default Obat;
