import { Route, Routes } from "react-router-dom";
import "./App.css";

import Home from "./pages/Home";
import Dashboard from "./components/Dashboard";
import PeriksaPasien from "./components/PeriksaPasien";
import DaftarGuru from "./components/DaftarGuru";
import DaftarSiswa from "./components/DaftarSiswa";
import DaftarKaryawan from "./components/DaftarKaryawan";
import PenangananPertama from "./components/PenangananPertama";
import Diagnosa from "./components/Diagnosa";
import Tindakan from "./components/Tindakan";
import Obat from "./components/Obat";
import Register from "./components/Register";
import Login from "./components/Login";
import Navbar from "./components/Navbar";
import UpdateGuru from "./components/update/UpdateGuru";
import UpdateSiswa from "./components/update/UpdateSiswa";
import UpdateKaryawan from "./components/update/UpdateKaryawan";
import UpdateDiagnosa from "./components/update/UpdateDiagnosa";
import UpdatePenanganan from "./components/update/UpdatePenanganan";
import UpdateTindakan from "./components/update/UpdateTindakan";
import UpdateObat from "./components/update/UpdateObat";
import Clock from "./components/Clock";
import Nav from "./components/Nav";

function App() {
  return (
    <div className="App">
      {/* <Navbar /> */}
      <Routes>
        <Route path="/" element={<Dashboard />} />
        {/* <Route path="/dashboard" element={<Dashboard />} /> */}
        <Route path="/periksapasien" element={<PeriksaPasien />} />
        <Route path="/daftarguru" element={<DaftarGuru />} />
        <Route path="/daftarsiswa" element={<DaftarSiswa />} />
        <Route path="/daftarkaryawan" element={<DaftarKaryawan />} />
        <Route path="/daftardiagnosa" element={<Diagnosa />} />
        <Route path="/daftarpenanganan" element={<PenangananPertama />} />
        <Route path="/daftartindakan" element={<Tindakan />} />
        <Route path="/daftarobat" element={<Obat />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/updateguru/:id" element={<UpdateGuru />} />
        <Route path="/updatesiswa/:id" element={<UpdateSiswa />} />
        <Route path="/updatekaryawan/:id" element={<UpdateKaryawan />} />
        <Route path="/updatediagnosa/:id" element={<UpdateDiagnosa />} />
        <Route path="/updatetindakan/:id" element={<UpdateTindakan />} />
        <Route path="/updatepenanganan/:id" element={<UpdatePenanganan />} />
        <Route path="/updateobat/:id" element={<UpdateObat />} />
      </Routes>
    </div>
  );
}

export default App;
